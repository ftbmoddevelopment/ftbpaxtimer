package com.fireball1725.ftb.PaxTimer.event;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.helpers.PlayerSaveFileHelper;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.event.entity.player.PlayerEvent;

import java.util.UUID;

public class EventPlayerEvent {
    @SubscribeEvent
    public void onPlayerSave(PlayerEvent.SaveToFile event) {
        PlayerSaveFileHelper.savePlayerData(event.entityPlayer);
    }

    @SubscribeEvent
    public void onPlayerLoad(PlayerEvent.LoadFromFile event) {
        GameTimer playerTimer = new GameTimer();

        EntityPlayerMP player = (EntityPlayerMP) event.entityPlayer;
        if (player == null)
            return;

        UUID playerUUID = player.getUniqueID();

        /* Checking to see if player timer exists already, if so delete it */
        if (PlayerTimer.getPlayerTimer(playerUUID) != null) {
            PlayerTimer.removeTimer(playerUUID);
        }

        // Load Player Timer
        NBTTagCompound nbtTagCompound = player.getEntityData();
        Boolean timerRunning = false;
        if (nbtTagCompound.hasKey("timerRunning")) {
            timerRunning = nbtTagCompound.getBoolean("timerRunning");
        }

        playerTimer.setupTimer(timerRunning);

        if (nbtTagCompound.hasKey("timerOffset")) {
            long offset = nbtTagCompound.getLong("timerOffset");
            playerTimer.resetClockWithOffset(offset);
        }

        PlayerTimer.addPlayerTimer(playerUUID, playerTimer);

        // Load Player Goals
        if (nbtTagCompound.hasKey("playerData")) {
            NBTTagCompound playerData = nbtTagCompound.getCompoundTag("playerData");
            PlayerGoals.loadSave(playerData, playerUUID);
        }

        // Load Player Location
        if (nbtTagCompound.hasKey("playerLocation")) {
            NBTTagCompound playerLocationData = nbtTagCompound.getCompoundTag("playerLocation");
            PlayerGoals.restorePlayerLastLocation(playerUUID, playerLocationData);
        }
    }

    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onPlayerRespawn(cpw.mods.fml.common.gameevent.PlayerEvent.PlayerRespawnEvent event) {
        EntityPlayer player = event.player;

        if (player == null)
            return;

        if (!(event.player instanceof EntityPlayerMP))
            return;

        EntityPlayerMP entityPlayerMP = (EntityPlayerMP) player;

        LogHelper.info("Teleporting Player");
        PlayerGoals.teleportPlayerLastLocation(entityPlayerMP);
    }

    @SubscribeEvent
    public void onPlayerTick(PlayerEvent.LivingUpdateEvent event) {
        Boolean playerCheating = false;

        if (event.entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer)event.entity;
            if (player.capabilities.isCreativeMode) {
                playerCheating = true;
            }

            // TODO: do something with this info...
        }
    }
}
