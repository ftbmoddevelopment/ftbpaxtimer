package com.fireball1725.ftb.PaxTimer.event;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import com.fireball1725.ftb.PaxTimer.util.Platform;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.util.UUID;

public class EventGameTimer implements Runnable {
    private long updateTime = 0;

    @Override
    public void run() {
        while (true) {
            try {
                boolean gamePaused = Minecraft.getMinecraft().isGamePaused();
                boolean gameSP = Minecraft.getMinecraft().isSingleplayer();
                boolean gameStarted = Minecraft.getMinecraft().thePlayer != null;
                boolean gameIServer = Minecraft.getMinecraft().isIntegratedServerRunning();
                boolean gameServer = Platform.isServer();

                EntityPlayer player = Minecraft.getMinecraft().thePlayer;

                if (player == null) {
                    Thread.sleep(50);
                    continue;
                }

                // If game is not single player, kill thread
                if (!gameSP) {
                    return;
                }

                UUID playerUUID = player.getUniqueID();
                GameTimer playerTimer = PlayerTimer.getPlayerTimer(playerUUID);

                if (playerTimer == null) {
                    continue;
                }

                boolean timerPaused = playerTimer.isTimerPaused();
                boolean timerRunning = playerTimer.isTimerRunning();

                // If game is single player, check to see if the game is paused...
                if (gameSP) {
                    if (timerRunning && !timerPaused && gamePaused) {
                        playerTimer.pauseTimer();
                        LogHelper.info("The game was paused, pausing the game timer");
                        updateGameTimer();
                        continue;
                    }

                    if (timerPaused && !gamePaused) {
                        playerTimer.resumeTimer();
                        LogHelper.info("The game was resumed, resuming the game timer");
                        updateGameTimer();
                        continue;
                    }
                }

                Thread.sleep(50);
            } catch (InterruptedException e) {
                LogHelper.fatal(e.getMessage());
            }
        }
    }

    /*
    Send out update of game timer to all clients
     */
    private void updateGameTimer() {
//        MessageSendGameSettings gameSettings = new MessageSendGameSettings();
//        gameSettings.gameDifficulty = FTBGoalTracker.instance.gameSettings.getGameDifficulty();
//
//        MessageSendGameClock gameClock = new MessageSendGameClock();
//
//
//        PacketHandler.INSTANCE.sendToAll(gameSettings);
    }
}
