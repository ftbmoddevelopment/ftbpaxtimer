package com.fireball1725.ftb.PaxTimer.event;

import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.core.goal.GoalBlock;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.data.DataSender;
import com.fireball1725.ftb.PaxTimer.data.payloads.ClientStatusPayloadPackage;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class EventNetwork implements Runnable {

    @Override
    public void run() {
        while (true) {
            try {
                //TODO: Send Network Packet, to let server know we are alive...
                Thread.sleep(5000); // TODO: Set this from configuration...

                ClientStatusPayloadPackage clientStatusPayloadPackage = new ClientStatusPayloadPackage();
                clientStatusPayloadPackage.setTimeStamp(System.currentTimeMillis());

                EntityPlayer player = Minecraft.getMinecraft().thePlayer;

                if (player != null) {
                    clientStatusPayloadPackage.setPlayerName(player.getDisplayName());
                    clientStatusPayloadPackage.setPlayerUUID(player.getUniqueID().toString());

                    UUID playerUUID = player.getUniqueID();

                    GameTimer gameTimer = PlayerTimer.getPlayerTimer(playerUUID);

                    if (gameTimer != null) {


                        clientStatusPayloadPackage.setGameStarted(gameTimer.isTimerRunning());
                        clientStatusPayloadPackage.setGameTimer(gameTimer.getTime());

                        ArrayList<ItemStack> playerGoals = GoalBlock.getPlayerGoals();

                        clientStatusPayloadPackage.setGoalsTotal(playerGoals.size());

                        int goalCompleteCount = 0;
                        for (ItemStack playerGoal : playerGoals) {
                            if (PlayerGoals.isGoalFound(playerUUID, playerGoal))
                                goalCompleteCount++;
                        }

                        clientStatusPayloadPackage.setGoalsFound(goalCompleteCount);

                        clientStatusPayloadPackage.setGameCompleted(gameTimer.isTimerRunning() == false && goalCompleteCount == playerGoals.size() ? true : false);
                    }
                }

                FTBPaxTimer.instance.networkPayload = clientStatusPayloadPackage;

                DataSender.sendData();
            } catch (Exception ex) {
                LogHelper.fatal(ex.getMessage());
            }
        }
    }
}
