package com.fireball1725.ftb.PaxTimer.event;

import com.fireball1725.ftb.PaxTimer.core.goal.GoalBlock;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.network.PacketHandler;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerGoals;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerTimer;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import cpw.mods.fml.common.network.FMLNetworkEvent;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class EventPlayer {
    public EventPlayer() {
        FMLCommonHandler.instance().bus().register(this);
    }

    @SubscribeEvent
    public void onClientConnectedToServerEvent(FMLNetworkEvent.ClientConnectedToServerEvent event) {
        PlayerTimer.clearTimers();
        PlayerGoals.deleteAllData();
        GoalBlock.clearGoalData();
    }

    @SubscribeEvent
    public void onPlayerLoggedInEvent(PlayerEvent.PlayerLoggedInEvent event) {
        EntityPlayerMP player = (EntityPlayerMP) event.player;

        if (player == null)
            return;

        UUID playerUUID = player.getUniqueID();

        GameTimer playerTimer = PlayerTimer.getPlayerTimer(playerUUID);

        if (playerTimer == null)
            return;

        // Send Player their Timer Data
        MessageSendPlayerTimer messageSendPlayerTimer = new MessageSendPlayerTimer(playerTimer.isTimerRunning(), playerTimer.isTimerPaused(), playerTimer.getTime());
        PacketHandler.INSTANCE.sendTo(messageSendPlayerTimer, player);

        // Send Player their Completed Goals
        ArrayList<ItemStack> playerGoals = GoalBlock.getPlayerGoals();

        for (ItemStack playerGoal : playerGoals) {
            if (PlayerGoals.isGoalFound(playerUUID, playerGoal)) {
                MessageSendPlayerGoals messageSendPlayerGoals = new MessageSendPlayerGoals(playerGoal);
                PacketHandler.INSTANCE.sendTo(messageSendPlayerGoals, player);
            }
        }

    }
}
