package com.fireball1725.ftb.PaxTimer.event;

import com.fireball1725.ftb.PaxTimer.client.gui.GuiShareToLanModified;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngameMenu;
import net.minecraftforge.client.event.GuiScreenEvent;

public class EventGuiWindows {
    @SubscribeEvent(priority = EventPriority.HIGHEST)
    public void onGuiScreenEvent(GuiScreenEvent.ActionPerformedEvent event) {
        if (event.gui instanceof GuiIngameMenu && event.button.id == 7) {
            GuiShareToLanModified guiShareToLanModified = new GuiShareToLanModified(Minecraft.getMinecraft().currentScreen);
            event.gui.mc.displayGuiScreen(guiShareToLanModified);
            event.button.func_146113_a(Minecraft.getMinecraft().getSoundHandler());
            event.setCanceled(true);
        }
    }

}
