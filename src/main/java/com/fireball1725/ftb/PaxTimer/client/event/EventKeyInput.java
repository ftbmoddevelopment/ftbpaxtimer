package com.fireball1725.ftb.PaxTimer.client.event;

import com.fireball1725.ftb.PaxTimer.client.ClientSettings;
import com.fireball1725.ftb.PaxTimer.proxy.ClientProxy;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.InputEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.settings.KeyBinding;

public class EventKeyInput {
    @SideOnly(Side.CLIENT)
    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public void onKeyPush(InputEvent.KeyInputEvent event) {
        KeyBinding[] keyBindings = ClientProxy.keyBindings;

        // Toggle HUD
        if (keyBindings[0].isPressed()) {
            ClientSettings.hudVisible = !ClientSettings.hudVisible;
        }
    }
}