package com.fireball1725.ftb.PaxTimer.client.render;

import com.fireball1725.ftb.PaxTimer.tileentity.controlblocks.TileEntityGoalBlock;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.StatCollector;
import net.minecraftforge.common.util.ForgeDirection;
import org.lwjgl.opengl.GL11;

public class TileGoalRenderer extends TileBaseRenderer {
    public static TileGoalRenderer _instance = null;

    public static TileGoalRenderer instance() {
        if (_instance == null)
            _instance = new TileGoalRenderer();
        return _instance;
    }

    @Override
    public void renderTileEntityAt(TileEntity tileEntity, double x, double y, double z, float p_147500_8_) {
        if (tileEntity instanceof TileEntityGoalBlock) {
            this.saveBoundTexture();

            int[][] savedGLState = modifyGLState(new int[]{GL11.GL_BLEND, GL11.GL_LIGHTING}, null);
            TileEntityGoalBlock tileEntityGoalBlock = (TileEntityGoalBlock) tileEntity;

            for (ForgeDirection forgeSide : ForgeDirection.VALID_DIRECTIONS) {
                boolean isTopBottom = forgeSide == ForgeDirection.DOWN || forgeSide == ForgeDirection.UP;
                this.setLight(tileEntityGoalBlock, forgeSide);

                if (forgeSide == ForgeDirection.DOWN || forgeSide == ForgeDirection.UP)
                    continue;

                if (tileEntityGoalBlock.getGoalItem() != null) {
                    this.renderStackOnBlock(tileEntityGoalBlock.getGoalItem(), forgeSide, forgeSide, x, y, z, 9.0F, 57.0F, isTopBottom ? 64.0F : 75.0F);
                    String blockName = StatCollector.translateToLocal(tileEntityGoalBlock.getGoalItem().getUnlocalizedName() + ".name");


                    this.renderTextOnBlock(blockName, forgeSide, forgeSide, x, y, z, 2.5F, 128.0F, 35.0F, 0.0F, 0);
                }
            }

            this.restoreGlState(savedGLState);
            this.loadBoundTexture();
        }
    }
}
