package com.fireball1725.ftb.PaxTimer.client.gui;

import com.fireball1725.ftb.PaxTimer.client.ClientSettings;
import com.fireball1725.ftb.PaxTimer.config.ConfigItemStack;
import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.core.goal.GoalBlock;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.helpers.MathHelper;
import com.fireball1725.ftb.PaxTimer.helpers.OverlayHelper;
import com.fireball1725.ftb.PaxTimer.item.Items;
import cpw.mods.fml.common.eventhandler.EventPriority;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.lwjgl.opengl.GL11;

import java.awt.*;
import java.util.ArrayList;
import java.util.UUID;


public class GuiHUDWindow {
    private final Minecraft minecraft;
    private OverlayHelper overlayHelper = new OverlayHelper();

    public GuiHUDWindow() {
        this.minecraft = Minecraft.getMinecraft();
    }

    @SubscribeEvent(priority = EventPriority.LOWEST)
    public void afterRenderGameOverlayEvent(RenderGameOverlayEvent.Post event) {
        if (!ClientSettings.hudVisible)
            return;

        if (event.type != RenderGameOverlayEvent.ElementType.ALL)
            return;

        int screenWidth = event.resolution.getScaledWidth();

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);

        EntityPlayer player = minecraft.thePlayer;
        UUID playerUUID = player.getUniqueID();

        com.fireball1725.ftb.PaxTimer.util.GameTimer playerTimer = PlayerTimer.getPlayerTimer(playerUUID);
        if (playerTimer != null) {
            String dateFormatted = DurationFormatUtils.formatDuration(playerTimer.getTime(), "HH:mm:ss.SSS");

            String overlayTitle = FTBPaxTimer.instance.configHelper.getHudTitle();
            String overlayPlayer = Minecraft.getMinecraft().thePlayer.getDisplayName();
            String overlayTimer = "Time: " + dateFormatted;
            String overlayServerFailure = StatCollector.translateToLocal("title.serverError");


            int overlayTitleWidth = this.minecraft.fontRenderer.getStringWidth(overlayTitle);
            int overlayTitleX = (screenWidth >> 1) - (overlayTitleWidth >> 1);
            int overlayPlayerWidth = this.minecraft.fontRenderer.getStringWidth(overlayPlayer);
            int overlayPlayerX = (screenWidth >> 1) - (overlayPlayerWidth >> 1);
            int overlayTimerWidth = this.minecraft.fontRenderer.getStringWidth(overlayTimer);
            int overlayTimerX = (screenWidth >> 1) - (overlayTimerWidth >> 1);
            int overlayGoalWidth = GoalBlock.getGoalCount() * 17;
            int overlayGoalX = (screenWidth >> 1) - (overlayGoalWidth >> 1);
            int overlayServerFailureWidth = this.minecraft.fontRenderer.getStringWidth(overlayServerFailure);
            int overlayServerFailureX = (screenWidth >> 1) - (overlayServerFailureWidth >> 1);

            int windowWidth = MathHelper.getLargestInt(overlayPlayerWidth, overlayTimerWidth, overlayTitleWidth, overlayGoalWidth);
            int windowX = (screenWidth >> 1) - (windowWidth >> 1);
            int windowY = 10;

            int colorBackground = new Color(56, 55, 69, 224).hashCode();
            int colorBorder = new Color(48, 41, 69).hashCode();
            int colorFont = new Color(255, 255, 255).hashCode();
            int errorFont = new Color(255, 64, 64).hashCode();

            // 42 for everything
            // 25 for no icons...
            int windowHeight = GoalBlock.getGoalCount() == 0 ? 25 : 42;
            windowHeight = FTBPaxTimer.instance.serverOffline ? windowHeight + 10 : windowHeight;

            overlayHelper.DrawWindowWithBorder(windowX, windowY, windowWidth, windowHeight, colorBackground, colorBorder);

            this.minecraft.fontRenderer.drawStringWithShadow(overlayTitle, overlayTitleX, windowY, colorFont);
            this.minecraft.fontRenderer.drawStringWithShadow(overlayPlayer, overlayPlayerX, windowY + 9, colorFont);
            this.minecraft.fontRenderer.drawStringWithShadow(overlayTimer, overlayTimerX, windowY + (9 * 2), colorFont);

            if (FTBPaxTimer.instance.serverOffline) {
                this.minecraft.fontRenderer.drawStringWithShadow(overlayServerFailure, overlayServerFailureX, windowY + (9 * 5), errorFont);
            }

            ArrayList<ItemStack> playerGoalsAll = GoalBlock.getPlayerGoals();
            ArrayList<ItemStack> playerGoals = new ArrayList<ItemStack>();

            ArrayList<ItemStack> configGoalListOrder = FTBPaxTimer.instance.configHelper.getGoalItems();

            for (ItemStack configGoal : configGoalListOrder) {
                for (ItemStack allGoals : playerGoalsAll) {
                    if (allGoals.isItemEqual(configGoal)) {
                        playerGoals.add(allGoals);
                    }
                }
            }

            int i = 0;
            for (ItemStack playerGoal : playerGoals) {
                if (PlayerGoals.isGoalFound(playerUUID, playerGoal)) {
                    overlayHelper.DrawItemStack(playerGoal, overlayGoalX + i, windowY + (9 * 3));
                } else {
                    overlayHelper.DrawItemStack(new ItemStack(Items.ITEM_ICON_QUESTIONMARK.item), overlayGoalX + i, windowY + (9 * 3));
                }

                i = i + 17;
            }
        }

        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
    }
}
