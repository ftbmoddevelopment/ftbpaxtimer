package com.fireball1725.ftb.PaxTimer.client.gui;

import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiLabel;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiShareToLan;
import net.minecraft.client.resources.I18n;

import java.lang.reflect.Field;

public class GuiShareToLanModified extends GuiShareToLan {
    public GuiShareToLanModified(GuiScreen p_i1055_1_) {
        super(p_i1055_1_);

        EntityPlayerSP player = Minecraft.getMinecraft().thePlayer;

        String gameMode = player.capabilities.allowEdit ? player.capabilities.isCreativeMode ? "creative" : "survival" : "adventure";

        try {
            Field field_146599_h = GuiShareToLan.class.getDeclaredField("field_146599_h");
            field_146599_h.setAccessible(true);

            field_146599_h.set(this, gameMode);
        } catch (Exception ex ) {
            LogHelper.fatal("Error during reflection");
            ex.printStackTrace();
        }
    }

    @Override
    public void initGui() {
        super.initGui();

        this.buttonList.remove(3); // Remove cheat button
        this.buttonList.remove(2); // Remove gamemode button
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRendererObj, I18n.format("lanServer.title", new Object[0]), this.width / 2, 50, 16777215);

        int k;

        for (k = 0; k < this.buttonList.size(); ++k)
        {
            ((GuiButton)this.buttonList.get(k)).drawButton(this.mc, p_73863_1_, p_73863_2_);
        }

        for (k = 0; k < this.labelList.size(); ++k)
        {
            ((GuiLabel)this.labelList.get(k)).func_146159_a(this.mc, p_73863_1_, p_73863_2_);
        }
    }
}
