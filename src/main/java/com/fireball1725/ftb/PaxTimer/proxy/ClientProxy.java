package com.fireball1725.ftb.PaxTimer.proxy;

import com.fireball1725.ftb.PaxTimer.client.gui.GuiHUDWindow;
import com.fireball1725.ftb.PaxTimer.client.render.TileGoalRenderer;
import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.event.EventGameTimer;
import com.fireball1725.ftb.PaxTimer.event.EventNetwork;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.fireball1725.ftb.PaxTimer.tileentity.controlblocks.TileEntityGoalBlock;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.common.MinecraftForge;
import org.lwjgl.input.Keyboard;

public class ClientProxy extends CommonProxy {
    public static KeyBinding[] keyBindings;

    @Override
    public void registerRenderers() {
        FTBPaxTimer.blockGoalBlockRendererID = RenderingRegistry.getNextAvailableRenderId();

        ClientRegistry.bindTileEntitySpecialRenderer(TileEntityGoalBlock.class, new TileGoalRenderer());
    }

    @Override
    public void registerGuis() {
        MinecraftForge.EVENT_BUS.register(new GuiHUDWindow());
    }

    @Override
    public void registerEvents() {
        super.registerEvents();
        new Thread(new EventGameTimer()).start();
        new Thread(new EventNetwork()).start();
    }

    @Override
    public void registerKeyBindings() {
        keyBindings = new KeyBinding[1];

        keyBindings[0] = new KeyBinding("key.toggleHud.desc", Keyboard.KEY_H, "key." + ModInfo.MOD_ID + ".category");

        for (int i = 0; i < keyBindings.length; ++i) {
            ClientRegistry.registerKeyBinding(keyBindings[i]);
        }
    }
}
