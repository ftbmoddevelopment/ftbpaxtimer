package com.fireball1725.ftb.PaxTimer.proxy;

import com.fireball1725.ftb.PaxTimer.block.Blocks;
import com.fireball1725.ftb.PaxTimer.client.event.EventKeyInput;
import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.event.*;
import com.fireball1725.ftb.PaxTimer.helpers.ConfigHelper;
import com.fireball1725.ftb.PaxTimer.item.Items;
import cpw.mods.fml.common.FMLCommonHandler;
import net.minecraftforge.common.MinecraftForge;

public abstract class CommonProxy implements IProxy {
    public void registerBlocks() {
        Blocks.registerAll();
    }

    public void registerItems() {
        Items.registerAll();
    }

    public void registerEvents() {
        //new ClientTickEvent();
        new EventServerTick();
        new EventPlayer();
        new EventWorld();

        EventPlayerEvent eventPlayerEvent = new EventPlayerEvent();

        FMLCommonHandler.instance().bus().register(new EventKeyInput());
        MinecraftForge.EVENT_BUS.register(eventPlayerEvent);
        FMLCommonHandler.instance().bus().register(eventPlayerEvent);
        MinecraftForge.EVENT_BUS.register(new EventGuiWindows());
    }

    public void registerRenderers() {

    }

    public void registerGuis() {

    }

    public void registerKeyBindings() {
        /* Client Side Only */
    }

    public void loadConfiguration() {
        FTBPaxTimer.instance.configHelper.LoadConfiguration();
    }
}
