package com.fireball1725.ftb.PaxTimer.proxy;

public interface IProxy {
    public abstract void registerBlocks();

    public abstract void registerItems();

    public abstract void registerEvents();

    public abstract void registerRenderers();

    public abstract void registerGuis();

    public abstract void registerKeyBindings();

    public abstract void loadConfiguration();
}
