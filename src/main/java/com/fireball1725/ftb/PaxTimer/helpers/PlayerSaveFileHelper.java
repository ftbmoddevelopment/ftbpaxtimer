package com.fireball1725.ftb.PaxTimer.helpers;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;

import java.util.UUID;

public class PlayerSaveFileHelper {
    public static void savePlayerData(EntityPlayer player) {
        GameTimer playerTimer;

        if (player == null)
            return;
        UUID playerUUID = player.getUniqueID();

        playerTimer = PlayerTimer.getPlayerTimer(playerUUID);

        if (playerTimer == null)
            return;

        NBTTagCompound nbtTagCompound = player.getEntityData();

        if (nbtTagCompound == null)
            return;

        nbtTagCompound.setLong("timerOffset", playerTimer.getTime());
        nbtTagCompound.setBoolean("timerRunning", playerTimer.isTimerRunning());
        nbtTagCompound.setBoolean("timerPaused", playerTimer.isTimerPaused());

        NBTTagCompound playerGoalData = PlayerGoals.savePlayerData(playerUUID);
        if (playerGoalData == null) {
            nbtTagCompound.removeTag("playerData");
        } else {
            nbtTagCompound.setTag("playerData", playerGoalData);
        }

        NBTTagCompound playerLocation = PlayerGoals.getPlayerLastLocation(playerUUID);
        if (playerLocation == null) {
            nbtTagCompound.removeTag("playerLocation");
        } else {
            nbtTagCompound.setTag("playerLocation", playerLocation);
        }
    }
}
