package com.fireball1725.ftb.PaxTimer.helpers;

import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.data.payloads.ClientStatusPayloadPackage;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonHelper {
    public static String generateJsonPayload() {
        Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
        String jsonData = gson.toJson(FTBPaxTimer.instance.networkPayload, ClientStatusPayloadPackage.class);

        return jsonData;
    }
}
