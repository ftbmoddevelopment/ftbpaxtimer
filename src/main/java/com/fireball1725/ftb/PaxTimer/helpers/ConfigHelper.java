package com.fireball1725.ftb.PaxTimer.helpers;

import com.fireball1725.ftb.PaxTimer.config.ConfigHudConfig;
import com.fireball1725.ftb.PaxTimer.config.ConfigItemStack;
import com.fireball1725.ftb.PaxTimer.config.ConfigMainConfig;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import cpw.mods.fml.common.Loader;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import java.io.*;
import java.util.ArrayList;

public class ConfigHelper {
    private ConfigMainConfig mainConfig;

    public ConfigHelper() {
        File configDirectory = new File(Loader.instance().getConfigDir() + File.separator + ModInfo.MOD_ID.toLowerCase() + File.separator);

        if (!configDirectory.exists()) {
            configDirectory.mkdir();
        }
    }

    public void LoadConfiguration() {
        try {
            Reader reader = new FileReader(Loader.instance().getConfigDir() + File.separator + ModInfo.MOD_ID.toLowerCase() + File.separator + "config.json");

            Gson gson = new GsonBuilder().create();
            mainConfig = gson.fromJson(reader, ConfigMainConfig.class);
        } catch (FileNotFoundException ex) {
            LogHelper.warn("Configuration file is not found, generating config file...");
            CreateConfigFile();
        } catch (JsonSyntaxException ex) {
            LogHelper.fatal("Configuration file is invalid, please delete or fix the config file then restart Minecraft...");
        } catch (Exception ex) {
            LogHelper.fatal("Unknown exception happened somehow?? not sure why??");
            ex.printStackTrace();
        }
    }

    public void CreateConfigFile() {
        try {
            Writer writer = new FileWriter(Loader.instance().getConfigDir() + File.separator + ModInfo.MOD_ID.toLowerCase() + File.separator + "config.json");

            Gson gson = new GsonBuilder().setPrettyPrinting().create();

            ConfigMainConfig configMainConfig = new ConfigMainConfig();
            configMainConfig.setFtbServerUrl("http://setmeplease.local");

            ConfigHudConfig configHudConfig = new ConfigHudConfig();
            configHudConfig.setHudTitle("FTB Pax Prime Tournament");

            configHudConfig.addGoalItem("minecraft:redstone");
            configHudConfig.addGoalItem("minecraft:redstone_torch");
            configHudConfig.addGoalItem("minecraft:redstone_block");

            configMainConfig.setConfigHudConfig(configHudConfig);

            gson.toJson(configMainConfig, ConfigMainConfig.class, writer);

            writer.close();
        } catch (IOException ex) {
            LogHelper.fatal("Error creating configuration file...");
            ex.printStackTrace();
        }
    }

    public String getFtbServerUrl() {
        return mainConfig.getFtbServerUrl();
    }

    public String getHudTitle() {
        ConfigHudConfig hudConfig = mainConfig.getConfigHudConfig();
        return hudConfig.getHudTitle();
    }

    public ArrayList<ItemStack> getGoalItems() {
        ConfigHudConfig hudConfig = mainConfig.getConfigHudConfig();

        ArrayList<ItemStack> configItems = new ArrayList<ItemStack>();

        for (ConfigItemStack configItem : hudConfig.getGoalItems()) {
            configItems.add(getItemStack(configItem.getGoalItem(), configItem.getGoalItemDamage()));
        }

        return configItems;
    }

    public ItemStack getItemStack(String itemName, int itemDamage) {
        Item item = (Item)Item.itemRegistry.getObject(itemName);

        ItemStack itemStack = new ItemStack(item);
        itemStack.setItemDamage(itemDamage);

        return itemStack;
    }
}
