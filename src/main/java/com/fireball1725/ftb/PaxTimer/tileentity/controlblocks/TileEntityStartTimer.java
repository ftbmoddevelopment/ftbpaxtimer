package com.fireball1725.ftb.PaxTimer.tileentity.controlblocks;

import com.fireball1725.ftb.PaxTimer.tileentity.TileEntityBase;
import net.minecraft.nbt.NBTTagCompound;

public class TileEntityStartTimer extends TileEntityBase {
    private double teleportX = 0;
    private double teleportY = 0;
    private double teleportZ = 0;
    private float teleportYaw = 0;
    private float teleportPitch = 0;
    private boolean programmed = false;

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);

        nbtTagCompound.setDouble("teleportX", teleportX);
        nbtTagCompound.setDouble("teleportY", teleportY);
        nbtTagCompound.setDouble("teleportZ", teleportZ);
        nbtTagCompound.setFloat("teleportYaw", teleportYaw);
        nbtTagCompound.setFloat("teleportPitch", teleportPitch);
        nbtTagCompound.setBoolean("teleportProgrammed", programmed);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);

        teleportX = nbtTagCompound.getDouble("teleportX");
        teleportY = nbtTagCompound.getDouble("teleportY");
        teleportZ = nbtTagCompound.getDouble("teleportZ");
        teleportYaw = nbtTagCompound.getFloat("teleportYaw");
        teleportPitch = nbtTagCompound.getFloat("teleportPitch");
        programmed = nbtTagCompound.getBoolean("teleportProgrammed");
    }

    public void setTeleportPosition(double x, double y, double z, float yaw, float pitch) {
        this.teleportX = x;
        this.teleportY = y;
        this.teleportZ = z;
        this.teleportYaw = yaw;
        this.teleportPitch = pitch;
        this.programmed = true;
    }

    public double getTeleportX() {
        return this.teleportX;
    }

    public double getTeleportY() {
        return this.teleportY;
    }

    public double getTeleportZ() {
        return this.teleportZ;
    }

    public float getTeleportYaw() {
        return this.teleportYaw;
    }

    public float getTeleportPitch() {
        return this.teleportPitch;
    }

    public boolean isProgrammed() {
        return this.programmed;
    }
}
