package com.fireball1725.ftb.PaxTimer.tileentity.controlblocks;

import com.fireball1725.ftb.PaxTimer.core.goal.GoalBlock;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.helpers.PlayerSaveFileHelper;
import com.fireball1725.ftb.PaxTimer.network.PacketHandler;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerGoal;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerTimer;
import com.fireball1725.ftb.PaxTimer.tileentity.TileEntityBase;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import com.fireball1725.ftb.PaxTimer.util.Platform;
import com.fireball1725.ftb.PaxTimer.util.PlayerTracker;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.util.ArrayList;
import java.util.UUID;

public class TileEntityGoalBlock extends TileEntityBase {
    private ItemStack goalItem = null;
    private int redstonePower = 0;
    private int redstoneTick = 0;

    public int getRedstonePower() {
        return this.redstonePower;
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        super.readFromNBT(nbtTagCompound);

        if (nbtTagCompound.hasKey("item")) {
            NBTTagList itemList = nbtTagCompound.getTagList("item", 10);
            NBTTagCompound itemTagCompound = itemList.getCompoundTagAt(0);
            goalItem = ItemStack.loadItemStackFromNBT(itemTagCompound);
        }

        registerGoal();
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound) {
        super.writeToNBT(nbtTagCompound);

        if (goalItem != null) {
            NBTTagList itemList = new NBTTagList();
            NBTTagCompound itemTagCompound = new NBTTagCompound();
            goalItem.writeToNBT(itemTagCompound);
            itemList.appendTag(itemTagCompound);

            nbtTagCompound.setTag("item", itemList);
        }
    }

    public ItemStack getGoalItem() {
        return this.goalItem;
    }

    public void setGoalItem(ItemStack goalItem) {
        this.goalItem = goalItem;
    }

    public void registerGoal() {
        // Register Goal Block
        if (goalItem != null) {
            GoalBlock.addPlayerGoal(goalItem);
        }
    }

    @Override
    public void updateEntity() {
        if (redstoneTick > 0) {
            redstoneTick--;

            if (redstoneTick == 0) {
                redstonePower = 0;
                markForUpdate();
            }
        }

        // TODO: Not sure if this is bad or not...
        //if (Platform.isClient())
        //return;

        if (goalItem == null)
            return;

        if (Platform.isServer()) {
            // TODO: Set distance by Config
            ArrayList<EntityPlayerMP> players = PlayerTracker.isPlayerNearBlock(xCoord, yCoord, zCoord, 3);

            if (players.size() == 0)
                return;

            for (int p = 0; p < players.size(); p++) {
                EntityPlayerMP player = players.get(p);

                boolean goalFound = PlayerGoals.isGoalFound(player.getUniqueID(), getGoalItem());
                if (!goalFound) {
                    markGoalComplete(player);
                }
            }
        }
    }

    public boolean markGoalComplete(EntityPlayer player) {
        UUID playerUUID = player.getUniqueID();
        GameTimer playerTimer = PlayerTimer.getPlayerTimer(playerUUID);

        if (playerTimer == null)
            return false;

        if (!playerTimer.isTimerRunning())
            return false;

        if (goalItem == null)
            return false;

        boolean goalFound = PlayerGoals.isGoalFound(player.getUniqueID(), getGoalItem());
        if (Platform.isServer() && !goalFound) {
            PlayerGoals.setGoalFound(player.getUniqueID(), getGoalItem(), 0L);

            MessageSendPlayerGoal messageSendPlayerGoal = new MessageSendPlayerGoal(goalItem);
            PacketHandler.INSTANCE.sendTo(messageSendPlayerGoal, (EntityPlayerMP) player);
            PlayerSaveFileHelper.savePlayerData(player);
            PlayerGoals.setPlayerLastLocation(player);
        }

        if (PlayerGoals.isAllPlayerGoalsFound(player.getUniqueID())) {
            playerTimer.stopTimer();
            PlayerSaveFileHelper.savePlayerData(player);
            MessageSendPlayerTimer messageSendPlayerTimer = new MessageSendPlayerTimer(playerTimer.isTimerRunning(), playerTimer.isTimerPaused(), playerTimer.getTime());
            PacketHandler.INSTANCE.sendTo(messageSendPlayerTimer, (EntityPlayerMP) player);
            player.playSound("mob.enderdragon.end", 1.0F, 1.0F);
            this.worldObj.playBroadcastSound(1018, xCoord, yCoord, zCoord, 0);
            int posX = this.worldObj.getSpawnPoint().posX;
            int posZ = this.worldObj.getSpawnPoint().posZ;
            int posY = this.worldObj.getHeightValue(posX, posZ) + 1;
            player.setPositionAndUpdate(posX, posY, posZ);
        }

        if (!goalFound) {
            this.redstonePower = 15;
            this.redstoneTick = 40;
            markForUpdate();
        }

        return true;
    }
}