package com.fireball1725.ftb.PaxTimer.network.message;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

public class MessageResetPlayerData implements IMessage, IMessageHandler<MessageResetPlayerData, IMessage> {
    @Override
    public void fromBytes(ByteBuf buf) {

    }

    @Override
    public void toBytes(ByteBuf buf) {

    }

    @Override
    @SideOnly(Side.CLIENT)
    public IMessage onMessage(MessageResetPlayerData message, MessageContext ctx) {
        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        PlayerGoals.deletePlayerData(player.getUniqueID());
        return null;
    }
}
