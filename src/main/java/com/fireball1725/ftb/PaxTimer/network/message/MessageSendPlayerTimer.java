package com.fireball1725.ftb.PaxTimer.network.message;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;

import java.util.UUID;

public class MessageSendPlayerTimer implements IMessage, IMessageHandler<MessageSendPlayerTimer, IMessage> {
    public boolean timerRunning;
    public boolean timerPaused;
    public long offset;

    public MessageSendPlayerTimer() {
    }

    public MessageSendPlayerTimer(boolean timerRunning, boolean timerPaused, long offset) {
        this.timerRunning = timerRunning;
        this.timerPaused = timerPaused;
        this.offset = offset;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.timerRunning = buf.readBoolean();
        this.timerPaused = buf.readBoolean();
        this.offset = buf.readLong();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeBoolean(this.timerRunning);
        buf.writeBoolean(this.timerPaused);
        buf.writeLong(this.offset);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IMessage onMessage(MessageSendPlayerTimer message, MessageContext ctx) {
        GameTimer playerTimer = new GameTimer();

        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        if (player == null)
            return null;

        UUID playerUUID = player.getUniqueID();

        if (PlayerTimer.getPlayerTimer(playerUUID) != null) {
            PlayerTimer.removeTimer(playerUUID);
        }

        playerTimer.setupTimer(message.timerRunning);
        playerTimer.resetClockWithOffset(message.offset);

        PlayerTimer.addPlayerTimer(playerUUID, playerTimer);

        return null;
    }
}
