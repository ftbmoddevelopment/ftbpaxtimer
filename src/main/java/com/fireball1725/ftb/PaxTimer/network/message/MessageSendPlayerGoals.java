package com.fireball1725.ftb.PaxTimer.network.message;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

public class MessageSendPlayerGoals implements IMessage, IMessageHandler<MessageSendPlayerGoals, IMessage> {
    public ItemStack goalItemStack;

    public MessageSendPlayerGoals() {

    }

    public MessageSendPlayerGoals(ItemStack goalItemStack) {
        this.goalItemStack = goalItemStack;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.goalItemStack = ByteBufUtils.readItemStack(buf);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        ByteBufUtils.writeItemStack(buf, this.goalItemStack);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IMessage onMessage(MessageSendPlayerGoals message, MessageContext ctx) {
        EntityPlayer player = Minecraft.getMinecraft().thePlayer;
        PlayerGoals.setGoalFound(player.getUniqueID(), message.goalItemStack, 0L);

        return null;
    }
}