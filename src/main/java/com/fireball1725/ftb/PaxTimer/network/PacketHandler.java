package com.fireball1725.ftb.PaxTimer.network;

import com.fireball1725.ftb.PaxTimer.network.message.MessageResetPlayerData;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerGoal;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerGoals;
import com.fireball1725.ftb.PaxTimer.network.message.MessageSendPlayerTimer;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import cpw.mods.fml.relauncher.Side;

public class PacketHandler {
    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(ModInfo.MOD_ID.toLowerCase());

    public static void init() {
        INSTANCE.registerMessage(MessageSendPlayerTimer.class, MessageSendPlayerTimer.class, 0, Side.CLIENT);
        INSTANCE.registerMessage(MessageSendPlayerGoal.class, MessageSendPlayerGoal.class, 1, Side.CLIENT);
        INSTANCE.registerMessage(MessageSendPlayerGoals.class, MessageSendPlayerGoals.class, 2, Side.CLIENT);
        INSTANCE.registerMessage(MessageResetPlayerData.class, MessageResetPlayerData.class, 3, Side.CLIENT);
    }
}
