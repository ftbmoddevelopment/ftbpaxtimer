package com.fireball1725.ftb.PaxTimer.item;

import com.fireball1725.ftb.PaxTimer.creativetab.ModCreativeTabs;
import com.fireball1725.ftb.PaxTimer.item.icons.ItemCheckMark;
import com.fireball1725.ftb.PaxTimer.item.icons.ItemQuestionMark;
import com.fireball1725.ftb.PaxTimer.item.logos.ItemFTBLogo;
import com.fireball1725.ftb.PaxTimer.item.tools.ItemTeleportTool;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.StatCollector;

public enum Items {
    ITEM_LOGO_FTBLOGO("logo.ftblogo", new ItemFTBLogo()),
    ITEM_ICON_QUESTIONMARK("icon.questionmark", new ItemQuestionMark()),
    ITEM_ICON_CHECKMARK("icon.checkmark", new ItemCheckMark()),

    ITEM_TOOL_TELEPORTTOOL("tool.teleporttool", new ItemTeleportTool(), ModCreativeTabs.FTBTimer),;

    private static boolean registered = false;
    public final Item item;
    private final String internalName;

    Items(String internalName, Item item) {
        this(internalName, item, null);
    }

    Items(String internalName, Item item, CreativeTabs creativeTabs) {
        this.internalName = internalName;
        this.item = item.setTextureName(ModInfo.MOD_ID + ":" + internalName);
        item.setUnlocalizedName(ModInfo.MOD_ID + "." + internalName);
        item.setCreativeTab(creativeTabs);
    }

    public static void registerAll() {
        if (registered)
            return;
        for (Items i : Items.values())
            i.register();
        registered = true;
    }

    private void register() {
        GameRegistry.registerItem(item, internalName);
    }

    public String getStatName() {
        return StatCollector.translateToLocal(item.getUnlocalizedName());
    }

    public ItemStack getStack(int damage, int size) {
        return new ItemStack(item, size, damage);
    }
}
