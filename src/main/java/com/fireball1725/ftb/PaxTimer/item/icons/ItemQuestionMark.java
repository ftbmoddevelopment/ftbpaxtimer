package com.fireball1725.ftb.PaxTimer.item.icons;

import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import net.minecraft.item.Item;

public class ItemQuestionMark extends Item {
    public ItemQuestionMark() {
        super();
        setTextureName(ModInfo.MOD_ID + ":icon.questionmark.png");
    }
}
