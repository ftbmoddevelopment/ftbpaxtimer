package com.fireball1725.ftb.PaxTimer.item.tools;

import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.fireball1725.ftb.PaxTimer.tileentity.controlblocks.TileEntityStartTimer;
import com.fireball1725.ftb.PaxTimer.util.TileTools;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;

import java.util.List;

public class ItemTeleportTool extends Item {
    public ItemTeleportTool() {
        super();
        setMaxStackSize(1);
        setTextureName(ModInfo.MOD_ID + ":tool.teleporttool.png");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer player) {
        if (player.isSneaking()) {
            if (itemStack.stackTagCompound == null) {
                itemStack.stackTagCompound = new NBTTagCompound();
            }

            itemStack.stackTagCompound.setDouble("teleportX", player.posX);
            itemStack.stackTagCompound.setDouble("teleportY", player.posY);
            itemStack.stackTagCompound.setDouble("teleportZ", player.posZ);
            itemStack.stackTagCompound.setFloat("teleportYaw", player.rotationYaw);
            itemStack.stackTagCompound.setFloat("teleportPitch", player.rotationPitch);

            player.playSound("random.successful_hit", 1.0F, 1.0F);
        }
        return itemStack;
    }

    @Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, int x, int y, int z, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_) {
        if (itemStack.stackTagCompound == null)
            return false;

        TileEntityStartTimer tileEntityStartTimer = TileTools.getTileEntity(world, x, y, z, TileEntityStartTimer.class);
        if (tileEntityStartTimer != null) {
            double posX = itemStack.stackTagCompound.getDouble("teleportX");
            double posY = itemStack.stackTagCompound.getDouble("teleportY");
            double posZ = itemStack.stackTagCompound.getDouble("teleportZ");
            float posYaw = itemStack.stackTagCompound.getFloat("teleportYaw");
            float posPitch = itemStack.stackTagCompound.getFloat("teleportPitch");
            tileEntityStartTimer.setTeleportPosition(posX, posY, posZ, posYaw, posPitch);

            player.playSound("random.successful_hit", 1.0F, 1.0F);
            return true;
        }

        return false;
    }

    @Override
    public void addInformation(ItemStack itemStack, EntityPlayer player, List list, boolean par4) {
        if (itemStack.stackTagCompound != null) {
            String teleportPos = EnumChatFormatting.GREEN + "Position: " + itemStack.stackTagCompound.getInteger("teleportX") + ", " + itemStack.stackTagCompound.getInteger("teleportY") + ", " + itemStack.stackTagCompound.getInteger("teleportZ");
            list.add(teleportPos);
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public boolean hasEffect(ItemStack itemStack) {
        return itemStack.stackTagCompound == null ? false : true;
    }

    @Override
    public EnumRarity getRarity(ItemStack itemStack) {
        return itemStack.stackTagCompound == null ? EnumRarity.common : EnumRarity.epic;
    }

}
