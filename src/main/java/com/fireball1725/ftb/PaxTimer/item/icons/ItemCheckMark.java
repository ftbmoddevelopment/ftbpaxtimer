package com.fireball1725.ftb.PaxTimer.item.icons;

import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import net.minecraft.item.Item;

public class ItemCheckMark extends Item {
    public ItemCheckMark() {
        super();
        setTextureName(ModInfo.MOD_ID + ":icon.checkmark.png");
    }
}
