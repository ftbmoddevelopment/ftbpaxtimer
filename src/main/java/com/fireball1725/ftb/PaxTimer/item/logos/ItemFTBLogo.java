package com.fireball1725.ftb.PaxTimer.item.logos;

import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import net.minecraft.item.Item;

public class ItemFTBLogo extends Item {
    public ItemFTBLogo() {
        super();
        setTextureName(ModInfo.MOD_ID + ":logo.ftblogo.png");
    }
}
