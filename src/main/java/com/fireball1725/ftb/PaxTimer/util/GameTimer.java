package com.fireball1725.ftb.PaxTimer.util;

import org.apache.commons.lang3.time.StopWatch;

public class GameTimer {
    private StopWatch stopWatch;
    private long offset = 0;

    public GameTimer() {
        stopWatch = new StopWatch();
    }

    public void startTimer() {
        stopWatch.start();
    }

    public void stopTimer() {
        stopWatch.stop();
    }

    public void pauseTimer() {
        if (stopWatch.isStarted() && !stopWatch.isSuspended())
            stopWatch.suspend();
    }

    public void resumeTimer() {
        if (stopWatch.isSuspended())
            stopWatch.resume();
    }

    public long getOffset() {
        return this.offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }

    public long getTime() {
        return this.offset + stopWatch.getTime();
    }

    public void setupTimer(boolean timerRunning) {
        if (timerRunning && !stopWatch.isStarted()) {
            stopWatch.start();
        }
        if (!timerRunning && stopWatch.isStarted()) {
            stopWatch.reset();
        }
    }

    public boolean isTimerRunning() {
        return stopWatch.isStarted();
    }

    public boolean isTimerPaused() {
        return stopWatch.isSuspended();
    }

    public void resetClockWithOffset(long offset) {
        boolean timerRunning = stopWatch.isStarted();

        this.offset = offset;
        stopWatch.reset();

        if (timerRunning)
            stopWatch.start();
    }

    public StopWatch getTimer() {
        return stopWatch;
    }
}
