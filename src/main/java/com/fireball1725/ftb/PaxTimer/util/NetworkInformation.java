package com.fireball1725.ftb.PaxTimer.util;

public class NetworkInformation {
    public static String networkHostname;
    public static String networkIPAddress;

    public static boolean networkHostnameFailure = false;
    public static boolean networkIPAddressFailure = false;
}
