package com.fireball1725.ftb.PaxTimer.util;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.AxisAlignedBB;

import java.util.ArrayList;
import java.util.ListIterator;

public class PlayerTracker {
    public static ArrayList<EntityPlayerMP> isPlayerNearBlock(int blockX, int blockY, int blockZ, int distance) {
        if (Platform.isClient())
            return null;

        ArrayList<EntityPlayerMP> playersInArea = new ArrayList<EntityPlayerMP>();

        AxisAlignedBB area = AxisAlignedBB.getBoundingBox(blockX, blockY, blockZ, blockX, blockY, blockZ).expand(distance, distance, distance);

        ArrayList<EntityPlayerMP> allPlayers = new ArrayList<EntityPlayerMP>();
        ListIterator itl;

        for (int i = 0; i < MinecraftServer.getServer().worldServers.length; i++) {
            itl = MinecraftServer.getServer().worldServers[i].playerEntities.listIterator();
            while (itl.hasNext()) {
                EntityPlayerMP playerMP = (EntityPlayerMP) itl.next();
                if (playerMP.boundingBox.intersectsWith(area))
                    playersInArea.add(playerMP);
            }
        }

        return playersInArea;
    }
}
