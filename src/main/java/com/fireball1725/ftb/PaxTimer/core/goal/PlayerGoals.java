package com.fireball1725.ftb.PaxTimer.core.goal;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;

import java.util.*;

public class PlayerGoals {
    private static HashMap<UUID, HashMap<ItemStack, Long>> playerGoals = new HashMap();
    private static HashMap<UUID, NBTTagCompound> playerLocation = new HashMap();

    public static void setPlayerLastLocation(EntityPlayer player) {
        NBTTagCompound playerLocationData = new NBTTagCompound();
        playerLocationData.setDouble("posX", player.posX);
        playerLocationData.setDouble("posY", player.posY);
        playerLocationData.setDouble("posZ", player.posZ);
        playerLocationData.setFloat("posYaw", player.rotationYaw);
        playerLocationData.setFloat("posPitch", player.rotationPitch);

        deletePlayerLastLocation(player.getUniqueID());

        playerLocation.put(player.getUniqueID(), playerLocationData);
    }

    public static void deletePlayerLastLocation(UUID playerUUID) {
        if (playerLocation.containsKey(playerUUID))
            playerLocation.remove(playerUUID);
    }

    public static void teleportPlayerLastLocation(EntityPlayerMP player) {
        if (!playerLocation.containsKey(player.getUniqueID()))
            return;

        NBTTagCompound playerLocationData = playerLocation.get(player.getUniqueID());

        if (playerLocation == null)
            return;

        player.playerNetServerHandler.setPlayerLocation(playerLocationData.getDouble("posX"), playerLocationData.getDouble("posY"), playerLocationData.getDouble("posZ"), playerLocationData.getFloat("posYaw"), playerLocationData.getFloat("posPitch"));
    }

    public static NBTTagCompound getPlayerLastLocation(UUID playerUUID) {
        if (!playerLocation.containsKey(playerUUID))
            return null;

        return playerLocation.get(playerUUID);
    }

    public static void restorePlayerLastLocation(UUID playerUUID, NBTTagCompound playerLocationData) {
        deletePlayerLastLocation(playerUUID);

        playerLocation.put(playerUUID, playerLocationData);
    }

    public static boolean isGoalFound(UUID playerUUID, ItemStack goalItem) {
        if (!playerGoals.containsKey(playerUUID))
            return false;

        HashMap<ItemStack, Long> playerGoalData = playerGoals.get(playerUUID);

        Iterator it = playerGoalData.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            if (((ItemStack) pair.getKey()).isItemEqual(goalItem))
                return true;
        }

        return false;
    }

    public static boolean isAllPlayerGoalsFound(UUID playerUUID) {
        if (!playerGoals.containsKey(playerUUID))
            return false;

        boolean allGoalsFound = true;
        ArrayList<ItemStack> playerGoals = GoalBlock.getPlayerGoals();

        for (ItemStack playerGoal : playerGoals) {
            if (!PlayerGoals.isGoalFound(playerUUID, playerGoal)) {
                allGoalsFound = false;
            }
        }

        return allGoalsFound;
    }

    public static boolean isPlayerStored(UUID playerUUID) {
        return playerGoals.containsKey(playerUUID);
    }

    public static Long getGoalFoundTime(UUID playerUUID, ItemStack goalItem) {
        if (isGoalFound(playerUUID, goalItem)) {
            HashMap<ItemStack, Long> playerGoalData = playerGoals.get(playerUUID);
            return playerGoalData.get(goalItem);
        }
        return null;
    }

    public static void setGoalFound(UUID playerUUID, ItemStack goalItem, Long goalTime) {
        if (!isPlayerStored(playerUUID)) {
            HashMap<ItemStack, Long> playerGoalData = new HashMap();
            playerGoalData.put(goalItem, goalTime);
            playerGoals.put(playerUUID, playerGoalData);
        } else {
            HashMap<ItemStack, Long> playerGoalData = playerGoals.get(playerUUID);
            playerGoalData.put(goalItem, goalTime);
            playerGoals.remove(playerUUID);
            playerGoals.put(playerUUID, playerGoalData);
        }
    }

    public static void deletePlayerData(UUID playerUUID) {
        if (isPlayerStored(playerUUID))
            playerGoals.remove(playerUUID);
    }

    public static void deleteAllData() {
        playerGoals.clear();
    }

    public static void loadSave(NBTTagCompound nbt, UUID playerUUID) {
        deletePlayerData(playerUUID);

        NBTTagList itemList = nbt.getTagList("playerData", 10);
        for (int i = 0; i < itemList.tagCount(); i++) {
            NBTTagCompound itemTag = itemList.getCompoundTagAt(i);
            setGoalFound(playerUUID, ItemStack.loadItemStackFromNBT(itemTag), itemTag.getLong("time"));
        }
    }

    public static NBTTagCompound savePlayerData(UUID playerUUID) {
        NBTTagCompound nbtTagCompound = new NBTTagCompound();

        if (!isPlayerStored(playerUUID))
            return null;

        HashMap<ItemStack, Long> playerGoalData = playerGoals.get(playerUUID);

        NBTTagList itemList = new NBTTagList();

        Iterator it = playerGoalData.entrySet().iterator();
        while (it.hasNext()) {
            NBTTagCompound itemTag = new NBTTagCompound();
            Map.Entry pair = (Map.Entry) it.next();
            ((ItemStack) pair.getKey()).writeToNBT(itemTag);
            itemTag.setLong("time", (Long) pair.getValue());
            itemList.appendTag(itemTag);
        }

        nbtTagCompound.setTag("playerData", itemList);

        return nbtTagCompound;
    }
}
