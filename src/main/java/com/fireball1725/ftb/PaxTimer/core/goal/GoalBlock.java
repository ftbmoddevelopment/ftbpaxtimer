package com.fireball1725.ftb.PaxTimer.core.goal;

import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;

public class GoalBlock {
    private static ArrayList<ItemStack> playerGoals = new ArrayList();
    private static ArrayList<ItemStack> playerGoalsOverlay = new ArrayList();

    // Used client side to clear goal data between worlds...
    public static void clearGoalData() {
        playerGoals.clear();
        playerGoalsOverlay.clear();
    }

    public static void addPlayerGoal(ItemStack itemGoal) {
        if (!playerGoalExist(itemGoal))
            playerGoalsOverlay.add(itemGoal);

        playerGoals.add(itemGoal);
        LogHelper.info("Registerd Goal - " + itemGoal.getDisplayName());
    }

    public static void removePlayerGoal(ItemStack itemGoal) {
        for (int i = 0; i < playerGoals.size(); i++) {
            if (playerGoals.get(i).isItemEqual(itemGoal)) {
                playerGoals.remove(i);
                LogHelper.info("Removed Goal - " + itemGoal.getDisplayName());
                break;
            }
        }

        if (!playerGoalExist(itemGoal)) {
            for (int i = 0; i < playerGoalsOverlay.size(); i++) {
                if (playerGoalsOverlay.get(i).isItemEqual(itemGoal)) {
                    playerGoalsOverlay.remove(i);
                    LogHelper.info("Removed Goal from Overlay - " + itemGoal.getDisplayName());
                    break;
                }
            }
        }
    }

    public static boolean playerGoalExist(ItemStack itemGoal) {
        for (ItemStack playerGoal : playerGoals) {
            if (playerGoal.isItemEqual(itemGoal))
                return true;
        }
        return false;
    }

    public static ArrayList<ItemStack> getPlayerGoals() {
        return playerGoalsOverlay;
    }

    public static int getGoalCount() {
        return playerGoalsOverlay.size();
    }
}
