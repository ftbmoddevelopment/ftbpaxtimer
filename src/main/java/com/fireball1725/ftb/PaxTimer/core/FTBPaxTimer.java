package com.fireball1725.ftb.PaxTimer.core;

import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.data.payloads.ClientStatusPayloadPackage;
import com.fireball1725.ftb.PaxTimer.helpers.ConfigHelper;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.network.PacketHandler;
import com.fireball1725.ftb.PaxTimer.proxy.IProxy;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.google.common.base.Stopwatch;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;

import java.util.concurrent.TimeUnit;

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, dependencies = ModInfo.DEPENDENCIES, version = ModInfo.VERSION_BUILD)
public class FTBPaxTimer {
    @Mod.Instance
    public static FTBPaxTimer instance;
    @SidedProxy(clientSide = ModInfo.CLIENT_PROXY_CLASS, serverSide = ModInfo.SERVER_PROXY_CLASS)
    public static IProxy proxy;
    public static int blockGoalBlockRendererID = -1;
    public static ClientStatusPayloadPackage networkPayload;
    public ConfigHelper configHelper = new ConfigHelper();
    public boolean serverOffline = false;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        LogHelper.info("Pre Initialization ( started )");

        PacketHandler.init();

        // Init Configuration
        proxy.loadConfiguration();

        // Register Blocks
        proxy.registerBlocks();

        // Register Items
        proxy.registerItems();

        // Register KeyBinds
        proxy.registerKeyBindings();

        LogHelper.info("Pre Initialization ( ended after " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + "ms )");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        LogHelper.info("Initialization ( started )");

        // Clear any timer data
        PlayerTimer.clearTimers();

        // Register Events
        proxy.registerEvents();

        // Register Renderers();
        proxy.registerRenderers();

        LogHelper.info("Initialization ( ended after " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + "ms )");

    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        // Register Gui Windows
        proxy.registerGuis();
    }

    @Mod.EventHandler
    public void onServerStartedEvent(FMLServerStartedEvent event) {
        PlayerTimer.clearTimers();
        PlayerGoals.deleteAllData();
    }
}
