package com.fireball1725.ftb.PaxTimer.core.goal;

import java.util.HashMap;
import java.util.UUID;

/*
    Game Timer Core
    Stores all the timers for all the players by UUID
 */

public class PlayerTimer {
    private static HashMap<UUID, com.fireball1725.ftb.PaxTimer.util.GameTimer> playerTimers = new HashMap();

    public static void addPlayerTimer(UUID playerUUID, com.fireball1725.ftb.PaxTimer.util.GameTimer gameTimer) {
        playerTimers.put(playerUUID, gameTimer);
    }

    public static com.fireball1725.ftb.PaxTimer.util.GameTimer getPlayerTimer(UUID playerUUID) {
        if (playerTimers.containsKey(playerUUID)) {
            return playerTimers.get(playerUUID);
        } else {
            return null;
        }
    }

    public static void clearTimers() {
        playerTimers.clear();
    }

    public static void removeTimer(UUID playerUUID) {
        playerTimers.remove(playerUUID);
    }
}
