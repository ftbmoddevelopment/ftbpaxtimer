package com.fireball1725.ftb.PaxTimer.data.payloads;

public class ClientStatusPayloadPackage {
    private long gameTimer;         // Game timer
    private boolean gameStarted;    // True = player is playing and timer is started
    private boolean gameCompleted;  // True = timer is stopped and all goals found
    private int goalsTotal;         // Total number of registered goals
    private int goalsFound;         // Total number of goals found by player
    private String playerName;      // Player's Name
    private String playerUUID;
    private long timeStamp;         // Time stamp of message

    public ClientStatusPayloadPackage() {

    }

    public void setGameTimer(long gameTimer) {
        this.gameTimer = gameTimer;
    }

    public void setGameStarted(boolean gameStarted) {
        this.gameStarted = gameStarted;
    }

    public void setGameCompleted(boolean gameCompleted) {
        this.gameCompleted = gameCompleted;
    }

    public void setGoalsTotal(int goalsTotal) {
        this.goalsTotal = goalsTotal;
    }

    public void setGoalsFound(int goalsFound) {
        this.goalsFound = goalsFound;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
