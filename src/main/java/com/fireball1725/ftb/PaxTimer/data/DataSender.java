package com.fireball1725.ftb.PaxTimer.data;

import com.fireball1725.ftb.PaxTimer.core.FTBPaxTimer;
import com.fireball1725.ftb.PaxTimer.data.response.EndpointResponse;
import com.fireball1725.ftb.PaxTimer.helpers.JsonHelper;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.InputStreamReader;

public class DataSender {
    private static int failCount = 0;

    public static void sendData() {
        String JsonData = JsonHelper.generateJsonPayload();

        if (FTBPaxTimer.instance.configHelper.getFtbServerUrl().equals("")) {
            FTBPaxTimer.instance.serverOffline = false;
            return;
        }

        try {
            HttpPost post = new HttpPost(FTBPaxTimer.instance.configHelper.getFtbServerUrl());
            post.setHeader("Content-type", "application/json");
            post.setEntity(new StringEntity(JsonData, "UTF-8"));
            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpResponse response = httpClient.execute(post);

            Gson gson = new GsonBuilder().create();
            EndpointResponse endPointResponse = gson.fromJson(new InputStreamReader(response.getEntity().getContent()), EndpointResponse.class);

            if (response.getStatusLine().getStatusCode() == 500) {
                if (failCount < 5) {
                    LogHelper.warn("HTTP error code 500 returned");
                    LogHelper.warn("Error count: " + failCount + " of 5");
                    FTBPaxTimer.instance.serverOffline = true;
                }
                failCount++;

            } else if (response.getStatusLine().getStatusCode() != 200) {
                if (failCount < 5) {
                    LogHelper.warn("HTTP error code " + response.getStatusLine().getStatusCode() + " returned, with message: " + endPointResponse.getError());
                    LogHelper.warn("Error count: " + failCount + " of 5");
                    FTBPaxTimer.instance.serverOffline = true;
                }
                failCount++;
            } else if (response.getStatusLine().getStatusCode() == 418) { // RFC 2324 Support
                LogHelper.fatal("I'm a little tea pot");
            } else if (response.getStatusLine().getStatusCode() == 200) {
                failCount = 0;
                FTBPaxTimer.instance.serverOffline = false;
            }
        } catch (Exception ex) {
            if (failCount < 5) {
                LogHelper.fatal("Fatal error while sending data");
                FTBPaxTimer.instance.serverOffline = true;
                ex.printStackTrace();
            }
            failCount++;
        }

        if (failCount == 5) {
            LogHelper.fatal("Failure to connect to server, will continue to try to connect in the background, supressing error messages...");
            FTBPaxTimer.instance.serverOffline = true;
        }
    }
}
