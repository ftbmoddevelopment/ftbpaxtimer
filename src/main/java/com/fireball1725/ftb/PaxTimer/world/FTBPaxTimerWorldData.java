package com.fireball1725.ftb.PaxTimer.world;

import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.WorldSavedData;

public class FTBPaxTimerWorldData extends WorldSavedData {
    private NBTTagCompound nbtTagCompound = new NBTTagCompound();

    public FTBPaxTimerWorldData(String tagName) {
        super(tagName);
    }

    @Override
    public void readFromNBT(NBTTagCompound nbtTagCompound) {
        this.nbtTagCompound = nbtTagCompound.getCompoundTag("FTB");
    }

    @Override
    public void writeToNBT(NBTTagCompound nbtTagCompound) {
        nbtTagCompound.setTag("FTB", this.nbtTagCompound);
    }

    public NBTTagCompound getNbtTagCompound() {
        return this.nbtTagCompound;
    }
}
