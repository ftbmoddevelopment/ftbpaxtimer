package com.fireball1725.ftb.PaxTimer.config;

public class ConfigMainConfig {
    private String ftbServerUrl;
    private ConfigHudConfig configHudConfig;

    public void setFtbServerUrl(String ftbServerUrl) {
        this.ftbServerUrl = ftbServerUrl;
    }

    public String getFtbServerUrl() {
        return this.ftbServerUrl;
    }

    public void setConfigHudConfig(ConfigHudConfig configHudConfig) {
        this.configHudConfig = configHudConfig;
    }

    public ConfigHudConfig getConfigHudConfig() {
        return this.configHudConfig;
    }
}
