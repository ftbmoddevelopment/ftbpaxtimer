package com.fireball1725.ftb.PaxTimer.config;

public class ConfigItemStack {
    private String goalItem = "minecraft:apple";
    private int goalItemDamage = 0;

    public void setGoalItem(String goalItem) {
        this.goalItem = goalItem;
    }

    public String getGoalItem() {
        return this.goalItem;
    }

    public void setGoalItemDamage(int goalItemDamage) {
        this.goalItemDamage = goalItemDamage;
    }

    public int getGoalItemDamage() {
        return goalItemDamage;
    }
}
