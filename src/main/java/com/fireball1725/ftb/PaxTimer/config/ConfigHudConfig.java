package com.fireball1725.ftb.PaxTimer.config;

import com.fireball1725.ftb.PaxTimer.util.ItemStackSrc;

import java.util.ArrayList;

public class ConfigHudConfig {
    private ArrayList<ConfigItemStack> goalItems = new ArrayList<ConfigItemStack>();
    private String hudTitle;

    public void setHudTitle(String hudTitle) {
        this.hudTitle = hudTitle;
    }

    public String getHudTitle() {
        return this.hudTitle;
    }

    public void addGoalItem(String goalItem, int damage) {
        ConfigItemStack configItemStack = new ConfigItemStack();
        configItemStack.setGoalItem(goalItem);
        configItemStack.setGoalItemDamage(damage);
        this.goalItems.add(configItemStack);
    }

    public void addGoalItem(String goalItem) {
        ConfigItemStack configItemStack = new ConfigItemStack();
        configItemStack.setGoalItem(goalItem);
        configItemStack.setGoalItemDamage(0);
        this.goalItems.add(configItemStack);
    }

    public void setGoalItems(ArrayList<ConfigItemStack> goalItems) {
        this.goalItems = goalItems;
    }

    public ArrayList<ConfigItemStack> getGoalItems() {
        return this.goalItems;
    }

    public ConfigItemStack getGoalItem(int goalItem) {
        return this.goalItems.get(goalItem);
    }

    public int getGoalItemCount () {
        return this.goalItems.size();
    }
}
