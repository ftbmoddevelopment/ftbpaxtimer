package com.fireball1725.ftb.PaxTimer.creativetab;

import com.fireball1725.ftb.PaxTimer.item.Items;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class ModCreativeTabs {
    public static final CreativeTabs FTBTimer = new CreativeTabs(ModInfo.MOD_ID) {
        @Override
        public Item getTabIconItem() {
            return Items.ITEM_LOGO_FTBLOGO.item;
        }
    };
}
