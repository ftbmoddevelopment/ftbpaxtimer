package com.fireball1725.ftb.PaxTimer.block.controlblocks;

import com.fireball1725.ftb.PaxTimer.block.BlockBase;
import com.fireball1725.ftb.PaxTimer.core.goal.GoalBlock;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.fireball1725.ftb.PaxTimer.tileentity.controlblocks.TileEntityGoalBlock;
import com.fireball1725.ftb.PaxTimer.util.Platform;
import com.fireball1725.ftb.PaxTimer.util.TileTools;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockGoalBlock extends BlockBase {
    @SideOnly(Side.CLIENT)
    IIcon iconSide;
    String internalName;

    public BlockGoalBlock() {
        super(Material.iron);
        setHardness(5.5F);
        setResistance(10.0F);
        setBlockUnbreakable();
        setStepSound(soundTypeMetal);
        setTileEntity(TileEntityGoalBlock.class);
    }

    @Override
    public boolean canProvidePower() {
        return true;
    }

    @Override
    public int isProvidingWeakPower(IBlockAccess world, int x, int y, int z, int side) {
        TileEntityGoalBlock tileEntityGoalBlock = TileTools.getTileEntity(world, x, y, z, TileEntityGoalBlock.class);

        if (tileEntityGoalBlock == null)
            return 0;

        return tileEntityGoalBlock.getRedstonePower();
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var2) {
        return new TileEntityGoalBlock();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        this.internalName = this.getUnlocalizedName().replace("tile." + ModInfo.MOD_ID + ".", "");

        iconSide = iconRegister.registerIcon(ModInfo.MOD_ID + ":" + "ControlBlock.TopBottom");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return this.iconSide;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntityGoalBlock tileEntityGoalBlock = TileTools.getTileEntity(world, x, y, z, TileEntityGoalBlock.class);

        if (tileEntityGoalBlock == null)
            return false;

        // Program goalblock if not programmed
        if (player.getHeldItem() != null && tileEntityGoalBlock.getGoalItem() == null) {
            if (Platform.isServer()) {
                ItemStack itemStack = player.getHeldItem();

                tileEntityGoalBlock.setGoalItem(itemStack);
                tileEntityGoalBlock.markForUpdate();
            }

            player.playSound("random.successful_hit", 1.0F, 1.0F);
            return true;
        }

        return false;
    }

    @Override
    public void breakBlock(World world, int x, int y, int z, Block block, int meta) {
        TileEntityGoalBlock tileEntityGoalBlock = TileTools.getTileEntity(world, x, y, z, TileEntityGoalBlock.class);

        if (tileEntityGoalBlock != null) {
            ItemStack goalItem = tileEntityGoalBlock.getGoalItem();

            if (goalItem != null) {
                GoalBlock.removePlayerGoal(goalItem);
            }
        }
    }
}
