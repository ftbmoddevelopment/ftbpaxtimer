package com.fireball1725.ftb.PaxTimer.block;

import com.fireball1725.ftb.PaxTimer.block.controlblocks.BlockGoalBlock;
import com.fireball1725.ftb.PaxTimer.block.controlblocks.BlockStartTimer;
import com.fireball1725.ftb.PaxTimer.creativetab.ModCreativeTabs;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.StatCollector;

public enum Blocks {
    BLOCK_STARTTIMER("StartTimer", new BlockStartTimer(), ModCreativeTabs.FTBTimer),
    //BLOCK_STOPTIMER("StopTimer", new BlockStopTimer(), ModCreativeTabs.FTBTimer),
    BLOCK_GOALBLOCK("GoalBlock", new BlockGoalBlock(), ModCreativeTabs.FTBTimer),;

    private static boolean registered = false;
    public final Block block;
    private final String internalName;
    private final Class<? extends ItemBlock> itemBlockClass;
    private final CreativeTabs creativeTabs;

    Blocks(String internalName, Block block) {
        this(internalName, block, ItemBlock.class, null);
    }

    Blocks(String internalName, Block block, CreativeTabs creativeTabs) {
        this(internalName, block, ItemBlock.class, creativeTabs);
    }

    Blocks(String internalName, Block block, Class<? extends ItemBlock> itemBlockClass) {
        this(internalName, block, itemBlockClass, null);
    }

    Blocks(String internalName, Block block, Class<? extends ItemBlock> itemBlockClass, CreativeTabs creativeTabs) {
        this.internalName = internalName;
        this.block = block;
        this.itemBlockClass = itemBlockClass;
        this.creativeTabs = creativeTabs;
        block.setBlockName(ModInfo.MOD_ID + "." + internalName);
    }

    public static void registerAll() {
        if (registered)
            return;
        for (Blocks b : Blocks.values())
            b.register();
        registered = true;
    }

    public String getInternalName() {
        return internalName;
    }

    public String getStatName() {
        return StatCollector.translateToLocal(block.getUnlocalizedName().replace("tileentity.", "block."));
    }

    public void register() {
        GameRegistry.registerBlock(block.setCreativeTab(creativeTabs).setBlockTextureName(ModInfo.MOD_ID + ":" + internalName), itemBlockClass, "tileentity." + internalName);
        LogHelper.info("Registered Block " + internalName);
    }
}
