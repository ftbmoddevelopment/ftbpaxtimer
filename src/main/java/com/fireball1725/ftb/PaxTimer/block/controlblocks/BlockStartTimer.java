package com.fireball1725.ftb.PaxTimer.block.controlblocks;

import com.fireball1725.ftb.PaxTimer.block.BlockBase;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerGoals;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.item.Items;
import com.fireball1725.ftb.PaxTimer.network.PacketHandler;
import com.fireball1725.ftb.PaxTimer.network.message.MessageResetPlayerData;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import com.fireball1725.ftb.PaxTimer.tileentity.controlblocks.TileEntityStartTimer;
import com.fireball1725.ftb.PaxTimer.util.GameTimer;
import com.fireball1725.ftb.PaxTimer.util.Platform;
import com.fireball1725.ftb.PaxTimer.util.TileTools;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.UUID;

public class BlockStartTimer extends BlockBase {
    @SideOnly(Side.CLIENT)
    IIcon iconTopBottom, iconSide;
    String internalName;

    public BlockStartTimer() {
        super(Material.iron);
        setHardness(5.5F);
        setResistance(10.0F);
        setBlockUnbreakable();
        setStepSound(soundTypeMetal);
        setTileEntity(TileEntityStartTimer.class);
    }

    @Override
    public TileEntity createNewTileEntity(World world, int var2) {
        return new TileEntityStartTimer();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        this.internalName = this.getUnlocalizedName().replace("tile." + ModInfo.MOD_ID + ".", "");

        iconTopBottom = iconRegister.registerIcon(ModInfo.MOD_ID + ":" + "ControlBlock.TopBottom");
        iconSide = iconRegister.registerIcon(ModInfo.MOD_ID + ":" + internalName);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return side == 1 ? this.iconTopBottom : side == 0 ? this.iconTopBottom : this.iconSide;
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        TileEntityStartTimer tileEntityStartTimer = TileTools.getTileEntity(world, x, y, z, TileEntityStartTimer.class);

        if (player.getHeldItem() != null && player.getHeldItem().getItem() == Items.ITEM_TOOL_TELEPORTTOOL.item)
            return false;

        if (tileEntityStartTimer == null)
            return false;

        if (!tileEntityStartTimer.isProgrammed())
            return false;

        PlayerGoals.deletePlayerData(player.getUniqueID());
        if (Platform.isServer()) {
            MessageResetPlayerData messageResetPlayerData = new MessageResetPlayerData();
            PacketHandler.INSTANCE.sendTo(messageResetPlayerData, (EntityPlayerMP) player);
        }

        player.setPositionAndRotation(tileEntityStartTimer.getTeleportX(), tileEntityStartTimer.getTeleportY(), tileEntityStartTimer.getTeleportZ(), tileEntityStartTimer.getTeleportYaw(), tileEntityStartTimer.getTeleportPitch());

        // Create timer, and start...
        UUID playerUUID = player.getUniqueID();
        GameTimer playerTimer = new GameTimer();
        playerTimer.startTimer();
        PlayerGoals.deletePlayerLastLocation(playerUUID);

        PlayerTimer.addPlayerTimer(playerUUID, playerTimer);

        return true;
    }
}
