package com.fireball1725.ftb.PaxTimer.block.controlblocks;

import com.fireball1725.ftb.PaxTimer.block.BlockBase;
import com.fireball1725.ftb.PaxTimer.core.goal.PlayerTimer;
import com.fireball1725.ftb.PaxTimer.helpers.LogHelper;
import com.fireball1725.ftb.PaxTimer.reference.ModInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.UUID;

public class BlockStopTimer extends BlockBase {
    @SideOnly(Side.CLIENT)
    IIcon iconTopBottom, iconSide;
    String internalName;

    public BlockStopTimer() {
        super(Material.iron);
        setHardness(5.5F);
        setResistance(10.0F);
        setStepSound(soundTypeMetal);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister iconRegister) {
        this.internalName = this.getUnlocalizedName().replace("tile." + ModInfo.MOD_ID + ".", "");

        iconTopBottom = iconRegister.registerIcon(ModInfo.MOD_ID + ":" + "ControlBlock.TopBottom");
        iconSide = iconRegister.registerIcon(ModInfo.MOD_ID + ":" + internalName);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int side, int meta) {
        return side == 1 ? this.iconTopBottom : side == 0 ? this.iconTopBottom : this.iconSide;
    }

    @Override
    public boolean onBlockActivated(World world, int p_149727_2_, int p_149727_3_, int p_149727_4_, EntityPlayer player, int p_149727_6_, float p_149727_7_, float p_149727_8_, float p_149727_9_) {
        UUID playerUUID = player.getUniqueID();
        com.fireball1725.ftb.PaxTimer.util.GameTimer playerTimer = PlayerTimer.getPlayerTimer(playerUUID);

        if (playerTimer == null)
            return false;

        if (!world.isRemote) {
            LogHelper.info("TEST ===> " + playerTimer.getTime() + "ms");
        }
        return true;
    }
}
