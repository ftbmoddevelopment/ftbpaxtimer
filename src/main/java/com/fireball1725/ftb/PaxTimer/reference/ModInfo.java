package com.fireball1725.ftb.PaxTimer.reference;

public class ModInfo {
    public static final String MOD_ID = "ftbpaxtimer";
    public static final String MOD_NAME = "FTB Pax Timer";
    public static final String VERSION_BUILD = "@VERSION@";
    public static final String VERSION_BUILD_NUMBER = "@BUILDNUMBER@";
    public static final String AUTOUPDATE_ID = "";
    public static final String DEPENDENCIES = "";
    public static final String SERVER_PROXY_CLASS = "com.fireball1725.ftb.PaxTimer.proxy.ServerProxy";
    public static final String CLIENT_PROXY_CLASS = "com.fireball1725.ftb.PaxTimer.proxy.ClientProxy";
}
